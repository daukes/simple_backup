#!/bin/bash
targetdir=($@)
tijd=$(date +%F)
if [[ $tijd == "" ]]; then
	exit
fi
mkdir -p "/backup/$tijd"
for d in ${targetdir[@]}; do
	cp -Rv "$d" "/backup/$tijd/"
done
tar zfc "/backup/$tijd.tar.gz" "/backup/$tijd/"
rm -rf "/backup/$tijd"
find /backup/ -type f -mtime +14 -name '*.gz' -delete